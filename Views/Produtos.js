'use strict';

const Produtos = {
    template: `<section>
                    <h2>Produtos</h2>
                    <h4>Cadastre um novo produto</h4>
                    <div class="field is-grouped" style="margin: 10px 0 20px 0;">                        
                        <div class="control is-expanded">
                            <input class="input" v-model="dados.descricao" type="text" placeholder="Descreva o produto">
                        </div>
                        <div class="control is-expanded">
                            <input class="input" v-model="dados.valor" type="text" placeholder="Defina um valor">
                        </div>
                        <div class="control">
                            <button class="button is-success" title="Cadastrar Novo Produto" @click="cadastraProduto" v-bind:class="{'is-loading' : isSubmited}">
                                <span class="icon is-small">
                                    <i class="fas fa-save"></i>
                                </span>
                            </button>
                        </div>
                    </div>
                    <button class="button is-danger" v-bind:disabled="trashSelected" @click="deletaProduto">
                            <span class="icon is-small">
                              <i class="fas fa-trash"></i>
                            </span>
                            <span>Apagar seleção</span>                            
                        </button>  
                    <table class="table is-hoverable is-striped is-fullwidth">
                        <thead>
                            <tr>
                                <th v-for="coluna in colunas">{{coluna.titulo}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="registro in registros" @click="trControl(registro.id_produto, $event)">
                                <td>{{registro.id_produto}}</td>
                                <td>{{registro.descricao}}</td>
                                <td>{{registro.valor}}</td>
                            </tr>
                        </tbody>
                    </table>
                </section>`,
    data() {
        return {
            resource: this.$resource('../endpoint.php?action=produto'),
            dados: [],
            registros: [],
            isSubmited: false,
            trSelected: [],
            trashSelected: true,
            colunas: [
                {titulo: "ID"},
                {titulo: "Descrição"},
                {titulo: "Valor"}
            ]
        }
    },
    methods: {
        initialize() {
            this.resource.get({}).then(function (response) {
                this.registros = response.data
            });
        },
        cadastraProduto: function () {

            this.isSubmited = true;

            let dados = {
                "descricao": this.dados.descricao,
                "valor": this.dados.valor
            };

            if (dados.descricao === undefined || dados.valor === undefined) {
                return false;
            }

            this.$http.post('../endpoint.php?action=produto', dados).then(function (response) {

                this.isSubmited = false;

                this.initialize();
            });
        },
        deletaProduto: function () {

            let a = 0;

            for (; a < this.trSelected.length; a++) {
                this.$http.delete('../endpoint.php?action=produto&id=' + this.trSelected[a]);
            }

            this.trashSelected = true;

            this.initialize();
        },
        trControl: function (id, e) {

            e.target.parentElement.classList.toggle('is-selected');

            if (!this.trSelected.includes(id)) {
                this.trSelected.push(id);
            }

            if (!e.target.parentElement.classList.contains('is-selected')) {
                this.trSelected.splice(this.trSelected.indexOf(id), 1);
            }

            if (this.trSelected.length >= 1) {
                this.trashSelected = false;
            }
            else {
                this.trashSelected = true;
            }
        }
    },
    created() {
        this.initialize()
    }
};