<?php

namespace Controllers;

use Models\Inserir;
use Models\Exibir;
use Models\Alterar;
use Models\Deletar;

class Produto
{

    private $dados;
    private $id;
    private $resultado;

    /**
     * Produto constructor.
     * @param string $metodo
     * @param array|null $dados
     * @param int|null $id
     */
    public function __construct(string $metodo, array $dados = null, int $id = null)
    {
        $this->dados = $dados ?? null;
        $this->id = $id ?? null;

        $this->{$metodo}();
    }

    public function resultado()
    {
        return $this->resultado;
    }

    private function cadatraProduto(): void
    {
        $Inserir = new Inserir("produto", $this->dados);

        if ($Inserir->resultado()):
            http_response_code(201);
        else:

            http_response_code(400);
        endif;
    }

    private function listaProduto(): void
    {
        $Exibir = new Exibir();
        $Exibir->exeExibir(null, "produto", null, null, false);

        $this->resultado = $Exibir->resultado();
    }

    private function editaProduto(): void
    {

    }

    private function apagaProduto(): void
    {
        $Deletar = new Deletar("produto", "WHERE id_produto = :id", "id={$this->id}");

        if ($Deletar->resultado()):
            http_response_code(201);
        else:
            http_response_code(400);
        endif;
    }
}