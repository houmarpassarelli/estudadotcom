'use strict';

const Clientes = {
    template: `<section>
                    <h2>Clientes</h2>                    
                    <div class="tabs is-boxed">
                        <ul>
                            <li @click="tabsControl('lista')" v-bind:class="{ 'is-active' : tab === 'lista'}">
                              <a>
                                <span class="icon is-small"><i class="fas fa-list-alt"></i></span>
                                <span>Lista de Clientes</span>
                              </a>
                            </li>
                            <li @click="tabsControl('cadastro')" v-bind:class="{ 'is-active' : tab === 'cadastro'}">
                              <a>
                                <span class="icon is-small"><i class="fas fa-save"></i></span>
                                <span>Cadastro de Clientes</span>
                              </a>
                            </li>
                        </ul>
                    </div>
                    <div v-if="tab === 'lista'">
                        <button class="button is-danger" v-bind:disabled="trashSelected" @click="deletaCliente">
                            <span class="icon is-small">
                              <i class="fas fa-trash"></i>
                            </span>
                            <span>Apagar seleção</span>                            
                        </button>                    
                        <table class="table is-hoverable is-striped is-fullwidth">
                            <thead>
                                <tr>
                                    <th v-for="coluna in colunas">{{coluna.titulo}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="registro in registros" @click="trControl(registro.id_cliente, $event)">
                                    <td>{{registro.id_cliente}}</td>
                                    <td>{{registro.nome}}</td>
                                    <td>{{registro.telefone}}</td>
                                    <td>{{registro.email}}</td>
                                    <td>{{registro.data_nascimento}}</td>
                                    <td>{{registro.genero | trataGenero}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div v-if="tab === 'cadastro'">
                        <div class="field">
                          <label class="label">Nome</label>
                          <div class="control has-icons-left has-icons-right">
                            <input v-model="dados.nome" class="input" type="text" placeholder="Digite o nome do cliente">
                            <span class="icon is-small is-left">
                              <i class="fas fa-user-tag"></i>
                            </span>
                            <span class="icon is-small is-right">
                              <i class="fas fa-exclamation-circle"></i>
                            </span>
                          </div>
                        </div>
                        <div class="field">
                          <label class="label">Telefone</label>
                          <div class="control has-icons-left">
                            <input v-model="dados.telefone" class="input" type="text" placeholder="Digite o telefone do cliente">
                            <span class="icon is-small is-left">
                              <i class="fas fa-phone"></i>
                            </span>
                          </div>
                        </div>                        
                        <div class="field">
                          <label class="label">Email</label>
                          <div class="control has-icons-left has-icons-right">
                            <input v-model="dados.email" class="input" type="email" placeholder="Digite o email do cliente">
                            <span class="icon is-small is-left">
                              <i class="fas fa-at"></i>
                            </span>
                            <span class="icon is-small is-right">
                              <i class="fas fa-exclamation-circle"></i>
                            </span>
                          </div>
                        </div>
                        <div class="field">
                            <label class="label">Gênero</label>
                            <div class="control">
                              <label class="radio">
                                <input type="radio" name="dados.genero">
                                Feminino
                              </label>
                              <label class="radio">
                                <input type="radio" name="dados.genero">
                                Masculino
                              </label>
                            </div>
                        </div>
                        <div class="field">
                            <button class="button is-danger is-outlined">
                                <span class="icon is-small">
                                  <i class="fas fa-times"></i>
                                </span>
                                <span>Limpar Campos</span>
                            </button>
                            <button class="button is-success" @click="cadastraCliente" v-bind:class="{'is-loading' : isSubmited}">
                                <span class="icon is-small">
                                  <i class="fas fa-save"></i>
                                </span>
                                <span>Cadastrar Cliente</span>
                            </button>
                        </div>
                    </div>                    
                </section>`,
    data() {
        return {
            resource: this.$resource('../endpoint.php?action=cliente'),
            dados: [],
            registros: [],
            tab: 'lista',
            isSubmited: false,
            trSelected: [],
            trashSelected: true,
            colunas: [
                {titulo: "ID"},
                {titulo: "Nome"},
                {titulo: "Telefone"},
                {titulo: "Email"},
                {titulo: "Data de Nascimento"},
                {titulo: "Gênero"}
            ]
        }
    },
    filters: {
        trataGenero: function (valor) {
            if (valor == 'M') {
                return 'Masculino';
            }
            else if (valor == 'F') {
                return 'Feminino';
            } else {
                return 'Não definido';
            }
        }
    },
    methods: {
        initialize() {
            this.resource.get({}).then(function (response) {
                this.registros = response.data
            });
        },
        cadastraCliente: function () {

            this.isSubmited = true;

            let dados = {
                "nome": this.dados.nome,
                "telefone": this.dados.telefone,
                "email": this.dados.email,
                "data_nascimento": this.dados.data_nascimento,
                "genero": this.dados.genero
            };

            if (dados.nome === undefined || dados.email === undefined) {
                return false;
            }

            this.$http.post('../endpoint.php?action=cliente', dados).then(function (response) {

                this.isSubmited = false;

                this.initialize();

            });
        },
        deletaCliente: function () {

            let a = 0;

            for (; a < this.trSelected.length; a++) {
                this.$http.delete('../endpoint.php?action=cliente&id=' + this.trSelected[a]);
            }

            this.trashSelected = true;

            this.initialize();
        },
        tabsControl: function (tab) {
            this.tab = tab;
        },
        trControl: function (id, e) {

            e.target.parentElement.classList.toggle('is-selected');

            if (!this.trSelected.includes(id)) {
                this.trSelected.push(id);
            }

            if (!e.target.parentElement.classList.contains('is-selected')) {
                this.trSelected.splice(this.trSelected.indexOf(id), 1);
            }

            if (this.trSelected.length >= 1) {
                this.trashSelected = false;
            }
            else {
                this.trashSelected = true;
            }
        }
    },
    created() {
        this.initialize()
    }
};