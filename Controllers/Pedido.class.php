<?php

namespace Controllers;

use Models\Inserir;
use Models\Exibir;
use Models\Alterar;
use Models\Deletar;

class Pedido
{
    private $dados;
    private $id;
    private $resultado;

    /**
     * Pedido constructor.
     * @param string $metodo
     * @param array|null $dados
     * @param int|null $id
     */
    public function __construct(string $metodo, array $dados = null, int $id = null)
    {
        $this->dados = $dados ?? null;
        $this->id = $id ?? null;

        $this->{$metodo}();
    }

    public function resultado()
    {
        return $this->resultado;
    }

    private function cadatraPedido(): void
    {

    }

    private function listaPedido(): void
    {

    }

    private function editaPedido(): void
    {

    }

    private function apagaPedido(): void
    {

    }
}