<?php

require 'config.inc.php';

use Controllers\Cliente;
use Controllers\Produto;
use Controllers\Pedido;

$input = null;

$request_method = $_SERVER['REQUEST_METHOD'];

parse_str($_SERVER['QUERY_STRING'], $query_string);

if ($request_method == 'POST'):

    $input = json_decode(file_get_contents('php://input'), true);

    switch ($query_string['action']):
        case 'cliente' :

            $data = new DateTime($input['data_nascimento']);

            $input['data_nascimento'] = $data->format('Y-m-d');

            (new Cliente('cadatraCliente', $input));

            break;
        case 'produto':

            (new Produto('cadatraProduto', $input));

            break;
    endswitch;

elseif ($request_method == 'GET'):

    $resultado = null;

    switch ($query_string['action']):
        case 'cliente' :

            $Cliente = new Cliente('listaCliente');

            $resultado = $Cliente->resultado();

            break;
        case 'produto':

            $Produto = new Produto('listaProduto');

            $resultado = $Produto->resultado();

            break;
    endswitch;

    file_put_contents('php://output', json_encode($resultado));

elseif ($request_method == 'PUT'):

elseif ($request_method == 'DELETE'):

    switch ($query_string['action']):
        case 'cliente' :

            (new Cliente('apagaCliente', null, $query_string['id']));

            break;
        case 'produto':

            (new Produto('apagaProduto', null, $query_string['id']));

            break;
    endswitch;

endif;