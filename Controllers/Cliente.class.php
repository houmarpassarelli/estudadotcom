<?php

namespace Controllers;

use Models\Inserir;
use Models\Exibir;
use Models\Alterar;
use Models\Deletar;

class Cliente
{
    private $dados;
    private $id;
    private $resultado;

    /**
     * Cliente constructor.
     * @param string $metodo
     * @param null|array $dados
     * @param null|int $id
     */
    public function __construct(string $metodo, array $dados = null, int $id = null)
    {
        $this->dados = $dados ?? null;
        $this->id = $id ?? null;

        $this->{$metodo}();
    }

    public function resultado()
    {
        return $this->resultado;
    }

    private function cadatraCliente(): void
    {
        $Inserir = new Inserir("cliente", $this->dados);

        if ($Inserir->resultado()):
            http_response_code(201);
        else:

            http_response_code(400);
        endif;
    }

    private function listaCliente(): void
    {

        $Exibir = new Exibir();
        $Exibir->exeExibir(null, "cliente", null, null, false);

        $this->resultado = $Exibir->resultado();

    }

    private function editaCliente(): void
    {

    }

    private function apagaCliente(): void
    {
        $Deletar = new Deletar("cliente", "WHERE id_cliente = :id", "id={$this->id}");

        if ($Deletar->resultado()):
            http_response_code(201);
        else:
            http_response_code(400);
        endif;
    }
}