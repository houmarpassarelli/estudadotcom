/**
 * Arquivo main.js
 *
 * Arquivo que faz o controle e inicia o router do VueJs, como também inicia o VueJs
 */

const routes = [
    { path: '/', component: Inicio},
    { path: '/clientes', component: Clientes },
    { path: '/produtos', component: Produtos },
    { path: '/pedidos', component: Pedidos }
];

const router = new VueRouter({
    routes
});

new Vue({
    router
}).$mount('#root');



