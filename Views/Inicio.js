'use strict';

const Inicio = {
    template: `<section>
                    <h1>Teste de seleção de desenvolvimento</h1>
                    <pre>
                        Sistema simples para cadastro de clientes, produtos e pedidos. <br />
                        Desenvolvido com PHP Orientado a Objetos no Back-End e VueJS no Front-End. <br />
                        Foi utilizado também metodologia MVC.<br />
                        <br />
                        Houmar Passareli
                    </pre>
                </section>` };