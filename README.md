# Teste de seleção de desenvolvimento - Estuda.com
## Houmar Passareli

Esse simples sistema foi desenvolvido em PHP Orientado a Objetos como Back-End
e VueJS no Front-End.

Foi Utilizada a metodologia MVC e os standards PSR no PHP.

---
## Banco de dados

O arquivo SQL está no repositório, basta importar para o MYSQL 5.7

1. Criar um banco de dados com o nome de estudaDOTcom

---
## Execução

Para rodar o sistema basta ter a versão do PHP 7.0 ou maior
e MYSQL 5.7, e seguir as informações abaixo:

1. Alterar o arquivo config.inc.php com as configurações do banco
2. Em seguida rode o comando para iniciar o servidor PHP **php -S localhost:8000**
3. Acessar sempre inicialmente pela url root **http://localhost:8000/**